XDG Virt Open
-------------------
Automatically boot libvirt machines and open URIs on it.

# Installation
**TODO**

# Requirements
 - bash
 - virsh
 - virt-viewer
 - netcat
 - kdialog
 - xdg-virt-guest

# License
[GNU General Public License 3.0](LICENSE)
